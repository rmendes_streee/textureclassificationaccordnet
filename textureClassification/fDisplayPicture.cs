﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace textureClassification
{
    public partial class fImgToProcess : Form
    {
        public textureVO imgToProcess { get; set; }

        public fImgToProcess()
        {
            InitializeComponent();
        }

        
        public void displayPictures()
        {
            textureGLCM GLCM = new textureGLCM();
            GLCM.imgDivision = cbImgDivision.SelectedItem == null ? cbImgDivision.Items[0].ToString() : cbImgDivision.SelectedItem.ToString();
            imgToProcess.splitBMP(GLCM.imgDivisionPixelParts);

            pImgToProcess.Controls.Clear();
            int x = 0;
            int y = 0;
            for (int i = 0; i < imgToProcess.imgBitmapSplited.Count(); i++)
            {
                PictureBox pb = new PictureBox();
                pb.Name = "pictureBox" + i;
                pb.BorderStyle = BorderStyle.FixedSingle;
                pb.Size = new Size(imgToProcess.imgBitmapSplited[i].Width, imgToProcess.imgBitmapSplited[i].Height);
                pb.Location = new Point(x, y);
                pb.Image = imgToProcess.imgBitmapSplited[i];
                pb.Visible = true;
                pb.Click += pb_Click;
                pImgToProcess.Controls.Add(pb);


                x = ((i > 0) && ((y + imgToProcess.imgBitmapSplited[i].Width) == imgToProcess.imgBitmap.Width)) ? x + imgToProcess.imgBitmapSplited[i].Width : x;
                y = ((i > 0) && ((y + imgToProcess.imgBitmapSplited[i].Width) == imgToProcess.imgBitmap.Width)) ? 0 : y + imgToProcess.imgBitmapSplited[i].Width;
            }

            
        }

        void pb_Click(object sender, EventArgs e)
        {
            PictureBox pb = (PictureBox)sender;
            if (pb.BackColor != SystemColors.Control)
            {
                pb.Height -= 6;
                pb.Width -= 6;
                pb.BackColor = SystemColors.Control;
                pb.SizeMode = PictureBoxSizeMode.CenterImage;
                fLPLearningSet.Controls.RemoveByKey("L"+pb.Name);
            }
            else
            {
                    PictureBox newPB = new PictureBox();
                    newPB.Name = "L" + pb.Name;
                    newPB.BorderStyle = BorderStyle.FixedSingle;
                    newPB.Size = pb.Size;
                    newPB.Image = pb.Image;
                    newPB.SizeMode = PictureBoxSizeMode.CenterImage;
                    newPB.Visible = true;

                    cDTrainingSet.ShowDialog();
                    newPB.BackColor = cDTrainingSet.Color;
                    newPB.Height += 10;
                    newPB.Width += 10;
                    fLPLearningSet.Controls.Add(newPB);

                    pb.Height += 6;
                    pb.Width += 6;
                    pb.BackColor = cDTrainingSet.Color;
                    pb.SizeMode = PictureBoxSizeMode.CenterImage;
            }
        }

        private void cbImgDivision_SelectedIndexChanged(object sender, EventArgs e)
        {
            fLPLearningSet.Controls.Clear();
            displayPictures();
        }

        private void fImgToProcess_Shown(object sender, EventArgs e)
        {
            cbImgDivision.SelectedIndex = 0;
            cbKNNDistance.SelectedIndex = 0;
        }

        private void btnCompute_Click(object sender, EventArgs e)
        {
            //GLCM Options
            textureGLCM glcm = new textureGLCM();
            glcm.dbPath = imgToProcess.dbPath;
            glcm.glcmDistance = (int)nUDDistance.Value;
            glcm.imgDivision = cbImgDivision.SelectedItem.ToString();
            glcm.KNNDistance = cbKNNDistance.SelectedItem.ToString();
            glcm.distanceK = (int)nUDdistanceK.Value;

            //Set Learning Set and To Be Classified Set
            glcm.learningSet = new List<textureVO>();
            glcm.toBeClassifiedSet = new List<textureVO>();
            foreach (PictureBox item in fLPLearningSet.Controls)
                glcm.learningSet.Add(new textureVO(imgToProcess.dbPath, imgToProcess.fileName, item.Name, item.BackColor, item.Image) );

            foreach (PictureBox item in pImgToProcess.Controls)
                glcm.toBeClassifiedSet.Add(new textureVO(imgToProcess.dbPath, imgToProcess.fileName, item.Name, item.Image));

            //Process Classification
            glcm.Compute();


            //Plot Classification
            int x = 0, y = 0, i = 0;
            foreach (textureVO item in glcm.toBeClassifiedSet)
            {
                foreach (PictureBox pb in pImgToProcess.Controls.Find(item.id, false))
                {
                    if (pb.BackColor == SystemColors.Control)
                    {
                        pb.Height += 8;
                        pb.Width += 8;
                    }
                    pb.Location = new Point(x + 15, y + 15);
                    pb.SizeMode = PictureBoxSizeMode.CenterImage;
                    pb.BackColor = glcm.colorClasses[item.classIndex];

                    x = ((i > 0) && ((y + imgToProcess.imgBitmapSplited[i].Width) == imgToProcess.imgBitmap.Width)) ? x + imgToProcess.imgBitmapSplited[i].Width : x;
                    y = ((i > 0) && ((y + imgToProcess.imgBitmapSplited[i].Width) == imgToProcess.imgBitmap.Width)) ? 0 : y + imgToProcess.imgBitmapSplited[i].Width;
                    i++;
                };
            }


        }
    }
}
