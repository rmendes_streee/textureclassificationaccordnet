﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace textureClassification
{
    public partial class fglcmTextureClassification : Form
    {
        public fglcmTextureClassification()
        {
            InitializeComponent();

            //textureClassification.textureGLCM.Compute();
            foldersFile();
            
        }

        

        

        public void foldersFile()
        {
            string[] dirs = Directory.GetFiles(@txtPathDB.Text);
            
            int i = 0;
            foreach (string dir in dirs)
            {
                string[] name = dir.ToString().Split('\\');
                Bitmap bmp = new Bitmap(@dir);
                bmp = new Bitmap(bmp, new Size(80, 80));
                Button btn = new Button();
                btn.Image = bmp;
                btn.Size = new Size(bmp.Width+15, bmp.Height+15);
                btn.Name = name[name.Count()-1];//.Split('.')[0];
                btn.Text = name[name.Count()-1];//.Split('.')[0];
                btn.ForeColor = Color.White;
                btn.Font = new Font("Microsoft Sans Serif", 10, FontStyle.Bold);
                btn.BackgroundImageLayout = ImageLayout.Zoom;
                btn.BackColor = Color.Gray;


                btn.Click += btn_Click;

                fLPImageDataBasePath.Controls.Add(btn);
                i++;
            }
        }

        void btn_Click(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            fImgToProcess fDisplayPic = new fImgToProcess();
            fDisplayPic.imgToProcess = new textureVO();
            fDisplayPic.imgToProcess.fileName = @txtPathDB.Text + "\\" + btn.Name;// +".bmp";
            fDisplayPic.imgToProcess.imgBitmap = new Bitmap(@fDisplayPic.imgToProcess.fileName);
            fDisplayPic.imgToProcess.dbPath = @txtPathDB.Text;
            fDisplayPic.Show();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            fLPImageDataBasePath.Controls.Clear();
            foldersFile();
        }

        private void btnPathDB_Click(object sender, EventArgs e)
        {
            fBDPathDB.ShowDialog();
            txtPathDB.Text = fBDPathDB.SelectedPath;
        }

        
    }
}
