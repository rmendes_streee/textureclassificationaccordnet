﻿namespace textureClassification
{
    partial class fglcmTextureClassification
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fglcmTextureClassification));
            this.pCentral = new System.Windows.Forms.Panel();
            this.panelRight = new System.Windows.Forms.Panel();
            this.lblIMGPath = new System.Windows.Forms.Label();
            this.btnPathDB = new System.Windows.Forms.Button();
            this.txtPathDB = new System.Windows.Forms.TextBox();
            this.btnCompute = new System.Windows.Forms.Button();
            this.lblOptions = new System.Windows.Forms.Label();
            this.panelLeft = new System.Windows.Forms.Panel();
            this.fLPImageDataBasePath = new System.Windows.Forms.FlowLayoutPanel();
            this.lblImgDBPath = new System.Windows.Forms.Label();
            this.cDTrainingSet = new System.Windows.Forms.ColorDialog();
            this.fBDPathDB = new System.Windows.Forms.FolderBrowserDialog();
            this.pCentral.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panelLeft.SuspendLayout();
            this.SuspendLayout();
            // 
            // pCentral
            // 
            this.pCentral.Controls.Add(this.panelRight);
            this.pCentral.Controls.Add(this.panelLeft);
            this.pCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pCentral.Location = new System.Drawing.Point(0, 0);
            this.pCentral.Name = "pCentral";
            this.pCentral.Size = new System.Drawing.Size(870, 524);
            this.pCentral.TabIndex = 5;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.lblIMGPath);
            this.panelRight.Controls.Add(this.btnPathDB);
            this.panelRight.Controls.Add(this.txtPathDB);
            this.panelRight.Controls.Add(this.btnCompute);
            this.panelRight.Controls.Add(this.lblOptions);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(548, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(322, 524);
            this.panelRight.TabIndex = 6;
            // 
            // lblIMGPath
            // 
            this.lblIMGPath.AutoSize = true;
            this.lblIMGPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIMGPath.Location = new System.Drawing.Point(8, 40);
            this.lblIMGPath.Name = "lblIMGPath";
            this.lblIMGPath.Size = new System.Drawing.Size(196, 20);
            this.lblIMGPath.TabIndex = 16;
            this.lblIMGPath.Text = "Image Data Base Path:";
            // 
            // btnPathDB
            // 
            this.btnPathDB.BackgroundImage = global::textureClassification.Properties.Resources.search;
            this.btnPathDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPathDB.Location = new System.Drawing.Point(272, 58);
            this.btnPathDB.Name = "btnPathDB";
            this.btnPathDB.Size = new System.Drawing.Size(33, 33);
            this.btnPathDB.TabIndex = 15;
            this.btnPathDB.UseVisualStyleBackColor = true;
            this.btnPathDB.Click += new System.EventHandler(this.btnPathDB_Click);
            // 
            // txtPathDB
            // 
            this.txtPathDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPathDB.Location = new System.Drawing.Point(8, 62);
            this.txtPathDB.Name = "txtPathDB";
            this.txtPathDB.Size = new System.Drawing.Size(258, 26);
            this.txtPathDB.TabIndex = 6;
            this.txtPathDB.Text = "C:\\brodatz\\bmp";
            // 
            // btnCompute
            // 
            this.btnCompute.AccessibleDescription = "Compute classification description";
            this.btnCompute.BackColor = System.Drawing.SystemColors.Control;
            this.btnCompute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCompute.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCompute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompute.Location = new System.Drawing.Point(38, 109);
            this.btnCompute.Name = "btnCompute";
            this.btnCompute.Size = new System.Drawing.Size(245, 52);
            this.btnCompute.TabIndex = 14;
            this.btnCompute.Tag = "Compute classification Tag";
            this.btnCompute.Text = "LOAD PATH";
            this.btnCompute.UseVisualStyleBackColor = false;
            this.btnCompute.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblOptions
            // 
            this.lblOptions.BackColor = System.Drawing.Color.LightGreen;
            this.lblOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptions.Location = new System.Drawing.Point(0, 0);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(322, 26);
            this.lblOptions.TabIndex = 6;
            this.lblOptions.Text = "::Options";
            // 
            // panelLeft
            // 
            this.panelLeft.Controls.Add(this.fLPImageDataBasePath);
            this.panelLeft.Controls.Add(this.lblImgDBPath);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(548, 524);
            this.panelLeft.TabIndex = 5;
            // 
            // fLPImageDataBasePath
            // 
            this.fLPImageDataBasePath.AutoScroll = true;
            this.fLPImageDataBasePath.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fLPImageDataBasePath.Location = new System.Drawing.Point(0, 26);
            this.fLPImageDataBasePath.Name = "fLPImageDataBasePath";
            this.fLPImageDataBasePath.Size = new System.Drawing.Size(548, 498);
            this.fLPImageDataBasePath.TabIndex = 2;
            // 
            // lblImgDBPath
            // 
            this.lblImgDBPath.BackColor = System.Drawing.Color.LightGreen;
            this.lblImgDBPath.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblImgDBPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgDBPath.Location = new System.Drawing.Point(0, 0);
            this.lblImgDBPath.Name = "lblImgDBPath";
            this.lblImgDBPath.Size = new System.Drawing.Size(548, 26);
            this.lblImgDBPath.TabIndex = 1;
            this.lblImgDBPath.Text = "::Image Data Base Path";
            // 
            // cDTrainingSet
            // 
            this.cDTrainingSet.FullOpen = true;
            this.cDTrainingSet.ShowHelp = true;
            this.cDTrainingSet.SolidColorOnly = true;
            // 
            // fglcmTextureClassification
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 524);
            this.Controls.Add(this.pCentral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fglcmTextureClassification";
            this.Text = "..:: GLCM Texture Classification ::..";
            this.pCentral.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            this.panelLeft.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pCentral;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Panel panelLeft;
        private System.Windows.Forms.Label lblImgDBPath;
        private System.Windows.Forms.FlowLayoutPanel fLPImageDataBasePath;
        private System.Windows.Forms.Label lblOptions;
        private System.Windows.Forms.Button btnCompute;
        private System.Windows.Forms.ColorDialog cDTrainingSet;
        private System.Windows.Forms.FolderBrowserDialog fBDPathDB;
        private System.Windows.Forms.Button btnPathDB;
        private System.Windows.Forms.TextBox txtPathDB;
        private System.Windows.Forms.Label lblIMGPath;


    }
}

