﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using AForge.Imaging;
using System.Data;
using System.Drawing.Imaging;

namespace textureClassification
{
    public class textureVO
    {
        public string           dbPath          { get; set; }
        public string           fileName        { get; set; }
        public string           id              { get; set; }
        public Color            color           { get; set; }
        public Bitmap           imgBitmap       { get; set; }
        public Bitmap[]         imgBitmapSplited{ get; set; }
        public UnmanagedImage   imgData         { get; set; }
        public UnmanagedImage[] imbDataSplited  { get; set; }
        public List<byte[]>     arraySplited    { get; set; }
        public int              classIndex      { get; set; }
        public DataTable        glcmDescriptors { get; set; }

        //generic
        public textureVO() { }

        //To be Classified
        public textureVO(string p_dbPath, string p_fileName, string p_id, System.Drawing.Image p_img)
        {
            dbPath = p_dbPath;
            fileName = p_fileName;
            id = p_id;
            color = SystemColors.Control;
            imgBitmap = new Bitmap(p_img).Clone(new Rectangle(0, 0, p_img.Width, p_img.Height), PixelFormat.Format8bppIndexed);

        }

        //Learning set
        public textureVO(string p_dbPath, string p_fileName, string p_id, Color p_color, System.Drawing.Image p_img)
        {
            dbPath = p_dbPath;
            fileName = p_fileName;
            id = p_id;
            color = p_color;
            imgBitmap = new Bitmap(p_img).Clone(new Rectangle(0, 0, p_img.Width, p_img.Height), PixelFormat.Format8bppIndexed);
        }

        public bool splitBMP(imagePixelParts p_parts)
        { 
            imgBitmapSplited = splitBMP(imgBitmap, p_parts);
            return (imgBitmapSplited.Count() > 0);
        }

        public static Bitmap[] splitBMP(Bitmap p_img, imagePixelParts numParts = imagePixelParts.pix320)
        {
            Bitmap[] result = new Bitmap[(int)numParts];
            int x = 0;
            int y = 0;
            int width = p_img.Width / Convert.ToInt32(Math.Sqrt((int)numParts));
            int height = p_img.Height / Convert.ToInt32(Math.Sqrt((int)numParts));

            for (int i = 0; i < (int)numParts; i++)
            {
                Rectangle rect = new Rectangle(x, y, width, height);
                Bitmap clone = p_img.Clone(rect, p_img.PixelFormat);
                result[i] = clone;

                x = ((i > 0) && ((y + width) == p_img.Width)) ? x + width : x;
                y = ((i > 0) && ((y + width) == p_img.Width)) ? 0 : y + width;
            }
            return result;
        }
    }
}
