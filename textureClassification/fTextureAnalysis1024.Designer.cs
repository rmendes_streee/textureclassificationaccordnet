﻿namespace textureClassification
{
    partial class fTextureAnalysis1024
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fTextureAnalysis1024));
            this.pDataBase = new System.Windows.Forms.Panel();
            this.fLPImageDataBasePath = new System.Windows.Forms.FlowLayoutPanel();
            this.lblIMGPath = new System.Windows.Forms.Label();
            this.btnPathDB = new System.Windows.Forms.Button();
            this.txtPathDB = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.pLeft = new System.Windows.Forms.Panel();
            this.fLPImgToProcess = new System.Windows.Forms.FlowLayoutPanel();
            this.pImgToProcess = new System.Windows.Forms.Panel();
            this.lblImgDBPath = new System.Windows.Forms.Label();
            this.panelRight = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbInternalDivision = new System.Windows.Forms.ComboBox();
            this.lblInternalDivision = new System.Windows.Forms.Label();
            this.btnClearProcess = new System.Windows.Forms.Button();
            this.btnClearLearningSet = new System.Windows.Forms.Button();
            this.lblKnnDistanceK = new System.Windows.Forms.Label();
            this.nUDdistanceK = new System.Windows.Forms.NumericUpDown();
            this.btnCompute = new System.Windows.Forms.Button();
            this.cbKNNDistance = new System.Windows.Forms.ComboBox();
            this.lblKNNDistance = new System.Windows.Forms.Label();
            this.cbImgDivision = new System.Windows.Forms.ComboBox();
            this.lblSplitImageSize = new System.Windows.Forms.Label();
            this.lblDistance = new System.Windows.Forms.Label();
            this.nUDDistance = new System.Windows.Forms.NumericUpDown();
            this.lblOptions = new System.Windows.Forms.Label();
            this.fLPLearningSet = new System.Windows.Forms.FlowLayoutPanel();
            this.lblLearningSet = new System.Windows.Forms.Label();
            this.pFundo = new System.Windows.Forms.Panel();
            this.fBDPathDB = new System.Windows.Forms.FolderBrowserDialog();
            this.cDTrainingSet = new System.Windows.Forms.ColorDialog();
            this.pDataBase.SuspendLayout();
            this.pLeft.SuspendLayout();
            this.fLPImgToProcess.SuspendLayout();
            this.panelRight.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDdistanceK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDistance)).BeginInit();
            this.SuspendLayout();
            // 
            // pDataBase
            // 
            this.pDataBase.BackColor = System.Drawing.Color.LightGreen;
            this.pDataBase.Controls.Add(this.fLPImageDataBasePath);
            this.pDataBase.Controls.Add(this.lblIMGPath);
            this.pDataBase.Controls.Add(this.btnPathDB);
            this.pDataBase.Controls.Add(this.txtPathDB);
            this.pDataBase.Controls.Add(this.button1);
            this.pDataBase.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pDataBase.Location = new System.Drawing.Point(0, 583);
            this.pDataBase.Name = "pDataBase";
            this.pDataBase.Size = new System.Drawing.Size(1283, 118);
            this.pDataBase.TabIndex = 0;
            // 
            // fLPImageDataBasePath
            // 
            this.fLPImageDataBasePath.AutoScroll = true;
            this.fLPImageDataBasePath.BackColor = System.Drawing.SystemColors.Control;
            this.fLPImageDataBasePath.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.fLPImageDataBasePath.Location = new System.Drawing.Point(0, 30);
            this.fLPImageDataBasePath.Name = "fLPImageDataBasePath";
            this.fLPImageDataBasePath.Size = new System.Drawing.Size(1283, 88);
            this.fLPImageDataBasePath.TabIndex = 21;
            // 
            // lblIMGPath
            // 
            this.lblIMGPath.AutoSize = true;
            this.lblIMGPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIMGPath.Location = new System.Drawing.Point(4, 6);
            this.lblIMGPath.Name = "lblIMGPath";
            this.lblIMGPath.Size = new System.Drawing.Size(174, 17);
            this.lblIMGPath.TabIndex = 20;
            this.lblIMGPath.Text = "Image Data Base Path:";
            // 
            // btnPathDB
            // 
            this.btnPathDB.BackgroundImage = global::textureClassification.Properties.Resources.search;
            this.btnPathDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPathDB.Location = new System.Drawing.Point(439, 5);
            this.btnPathDB.Name = "btnPathDB";
            this.btnPathDB.Size = new System.Drawing.Size(23, 23);
            this.btnPathDB.TabIndex = 19;
            this.btnPathDB.UseVisualStyleBackColor = true;
            this.btnPathDB.Click += new System.EventHandler(this.btnPathDB_Click);
            // 
            // txtPathDB
            // 
            this.txtPathDB.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPathDB.Location = new System.Drawing.Point(177, 4);
            this.txtPathDB.Name = "txtPathDB";
            this.txtPathDB.Size = new System.Drawing.Size(258, 23);
            this.txtPathDB.TabIndex = 17;
            this.txtPathDB.Text = "..\\..\\..\\BrodatzGray";
            // 
            // button1
            // 
            this.button1.AccessibleDescription = "Compute classification description";
            this.button1.BackColor = System.Drawing.SystemColors.Control;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(472, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 22);
            this.button1.TabIndex = 18;
            this.button1.Tag = "Compute classification Tag";
            this.button1.Text = "LOAD PATH";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pLeft
            // 
            this.pLeft.BackColor = System.Drawing.SystemColors.Control;
            this.pLeft.Controls.Add(this.fLPImgToProcess);
            this.pLeft.Controls.Add(this.lblImgDBPath);
            this.pLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.pLeft.Location = new System.Drawing.Point(0, 0);
            this.pLeft.Name = "pLeft";
            this.pLeft.Size = new System.Drawing.Size(663, 583);
            this.pLeft.TabIndex = 1;
            // 
            // fLPImgToProcess
            // 
            this.fLPImgToProcess.AutoScroll = true;
            this.fLPImgToProcess.BackColor = System.Drawing.SystemColors.Control;
            this.fLPImgToProcess.Controls.Add(this.pImgToProcess);
            this.fLPImgToProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fLPImgToProcess.Location = new System.Drawing.Point(0, 20);
            this.fLPImgToProcess.Name = "fLPImgToProcess";
            this.fLPImgToProcess.Size = new System.Drawing.Size(663, 563);
            this.fLPImgToProcess.TabIndex = 3;
            // 
            // pImgToProcess
            // 
            this.pImgToProcess.BackColor = System.Drawing.SystemColors.Control;
            this.pImgToProcess.Location = new System.Drawing.Point(3, 3);
            this.pImgToProcess.Name = "pImgToProcess";
            this.pImgToProcess.Size = new System.Drawing.Size(640, 640);
            this.pImgToProcess.TabIndex = 1;
            // 
            // lblImgDBPath
            // 
            this.lblImgDBPath.BackColor = System.Drawing.Color.LightGreen;
            this.lblImgDBPath.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblImgDBPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgDBPath.Location = new System.Drawing.Point(0, 0);
            this.lblImgDBPath.Name = "lblImgDBPath";
            this.lblImgDBPath.Size = new System.Drawing.Size(663, 20);
            this.lblImgDBPath.TabIndex = 2;
            this.lblImgDBPath.Text = "::Image To Process";
            // 
            // panelRight
            // 
            this.panelRight.BackColor = System.Drawing.SystemColors.Control;
            this.panelRight.Controls.Add(this.panel1);
            this.panelRight.Controls.Add(this.fLPLearningSet);
            this.panelRight.Controls.Add(this.lblLearningSet);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(663, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(620, 583);
            this.panelRight.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.Controls.Add(this.cbInternalDivision);
            this.panel1.Controls.Add(this.lblInternalDivision);
            this.panel1.Controls.Add(this.btnClearProcess);
            this.panel1.Controls.Add(this.btnClearLearningSet);
            this.panel1.Controls.Add(this.lblKnnDistanceK);
            this.panel1.Controls.Add(this.nUDdistanceK);
            this.panel1.Controls.Add(this.btnCompute);
            this.panel1.Controls.Add(this.cbKNNDistance);
            this.panel1.Controls.Add(this.lblKNNDistance);
            this.panel1.Controls.Add(this.cbImgDivision);
            this.panel1.Controls.Add(this.lblSplitImageSize);
            this.panel1.Controls.Add(this.lblDistance);
            this.panel1.Controls.Add(this.nUDDistance);
            this.panel1.Controls.Add(this.lblOptions);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 150);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(620, 433);
            this.panel1.TabIndex = 28;
            // 
            // cbInternalDivision
            // 
            this.cbInternalDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbInternalDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbInternalDivision.FormattingEnabled = true;
            this.cbInternalDivision.Items.AddRange(new object[] {
            "n1",
            "n4",
            "n16",
            "n64",
            "n256",
            "n1024",
            "n4096",
            "n16384"});
            this.cbInternalDivision.Location = new System.Drawing.Point(157, 92);
            this.cbInternalDivision.Name = "cbInternalDivision";
            this.cbInternalDivision.Size = new System.Drawing.Size(121, 24);
            this.cbInternalDivision.TabIndex = 40;
            // 
            // lblInternalDivision
            // 
            this.lblInternalDivision.AutoSize = true;
            this.lblInternalDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInternalDivision.Location = new System.Drawing.Point(31, 95);
            this.lblInternalDivision.Name = "lblInternalDivision";
            this.lblInternalDivision.Size = new System.Drawing.Size(125, 17);
            this.lblInternalDivision.TabIndex = 39;
            this.lblInternalDivision.Text = "Internal Division";
            // 
            // btnClearProcess
            // 
            this.btnClearProcess.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearProcess.Location = new System.Drawing.Point(151, 184);
            this.btnClearProcess.Name = "btnClearProcess";
            this.btnClearProcess.Size = new System.Drawing.Size(118, 52);
            this.btnClearProcess.TabIndex = 38;
            this.btnClearProcess.Text = "CLEAR P";
            this.btnClearProcess.UseVisualStyleBackColor = true;
            this.btnClearProcess.Click += new System.EventHandler(this.btnClearProcess_Click);
            // 
            // btnClearLearningSet
            // 
            this.btnClearLearningSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClearLearningSet.Location = new System.Drawing.Point(24, 184);
            this.btnClearLearningSet.Name = "btnClearLearningSet";
            this.btnClearLearningSet.Size = new System.Drawing.Size(121, 52);
            this.btnClearLearningSet.TabIndex = 37;
            this.btnClearLearningSet.Text = "CLEAR LS";
            this.btnClearLearningSet.UseVisualStyleBackColor = true;
            this.btnClearLearningSet.Click += new System.EventHandler(this.btnClearLearningSet_Click);
            // 
            // lblKnnDistanceK
            // 
            this.lblKnnDistanceK.AutoSize = true;
            this.lblKnnDistanceK.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKnnDistanceK.Location = new System.Drawing.Point(64, 157);
            this.lblKnnDistanceK.Name = "lblKnnDistanceK";
            this.lblKnnDistanceK.Size = new System.Drawing.Size(86, 17);
            this.lblKnnDistanceK.TabIndex = 36;
            this.lblKnnDistanceK.Text = "Distance K";
            // 
            // nUDdistanceK
            // 
            this.nUDdistanceK.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nUDdistanceK.Location = new System.Drawing.Point(158, 155);
            this.nUDdistanceK.Maximum = new decimal(new int[] {
            640,
            0,
            0,
            0});
            this.nUDdistanceK.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDdistanceK.Name = "nUDdistanceK";
            this.nUDdistanceK.ReadOnly = true;
            this.nUDdistanceK.Size = new System.Drawing.Size(121, 23);
            this.nUDdistanceK.TabIndex = 35;
            this.nUDdistanceK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nUDdistanceK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnCompute
            // 
            this.btnCompute.AccessibleDescription = "Compute classification description";
            this.btnCompute.BackColor = System.Drawing.SystemColors.Control;
            this.btnCompute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCompute.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCompute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompute.Location = new System.Drawing.Point(24, 242);
            this.btnCompute.Name = "btnCompute";
            this.btnCompute.Size = new System.Drawing.Size(245, 52);
            this.btnCompute.TabIndex = 34;
            this.btnCompute.Tag = "Compute classification Tag";
            this.btnCompute.Text = "PROCESS";
            this.btnCompute.UseVisualStyleBackColor = false;
            this.btnCompute.Click += new System.EventHandler(this.btnCompute_Click);
            // 
            // cbKNNDistance
            // 
            this.cbKNNDistance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKNNDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbKNNDistance.FormattingEnabled = true;
            this.cbKNNDistance.Items.AddRange(new object[] {
            "Euclidean",
            "BrayCurtis",
            "Canberra",
            "Chessboard",
            "Correlation",
            "Cosine",
            "Manhattan",
            "SquareEuclidean",
            "Hamming",
            "Chebyshev"});
            this.cbKNNDistance.Location = new System.Drawing.Point(158, 122);
            this.cbKNNDistance.Name = "cbKNNDistance";
            this.cbKNNDistance.Size = new System.Drawing.Size(121, 24);
            this.cbKNNDistance.TabIndex = 33;
            // 
            // lblKNNDistance
            // 
            this.lblKNNDistance.AutoSize = true;
            this.lblKNNDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKNNDistance.Location = new System.Drawing.Point(6, 127);
            this.lblKNNDistance.Name = "lblKNNDistance";
            this.lblKNNDistance.Size = new System.Drawing.Size(149, 17);
            this.lblKNNDistance.TabIndex = 32;
            this.lblKNNDistance.Text = "KNN Distance Type";
            // 
            // cbImgDivision
            // 
            this.cbImgDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbImgDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbImgDivision.FormattingEnabled = true;
            this.cbImgDivision.Items.AddRange(new object[] {
            "pix640",
            "pix320",
            "pix160",
            "pix80",
            "pix40",
            "pix20",
            "pix10",
            "pix5"});
            this.cbImgDivision.Location = new System.Drawing.Point(158, 61);
            this.cbImgDivision.Name = "cbImgDivision";
            this.cbImgDivision.Size = new System.Drawing.Size(121, 24);
            this.cbImgDivision.TabIndex = 31;
            this.cbImgDivision.SelectedIndexChanged += new System.EventHandler(this.cbImgDivision_SelectedIndexChanged);
            // 
            // lblSplitImageSize
            // 
            this.lblSplitImageSize.AutoSize = true;
            this.lblSplitImageSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSplitImageSize.Location = new System.Drawing.Point(7, 64);
            this.lblSplitImageSize.Name = "lblSplitImageSize";
            this.lblSplitImageSize.Size = new System.Drawing.Size(149, 17);
            this.lblSplitImageSize.TabIndex = 30;
            this.lblSplitImageSize.Text = "Image Division Size";
            // 
            // lblDistance
            // 
            this.lblDistance.AutoSize = true;
            this.lblDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDistance.Location = new System.Drawing.Point(35, 34);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(119, 17);
            this.lblDistance.TabIndex = 29;
            this.lblDistance.Text = "GLCM Distance";
            // 
            // nUDDistance
            // 
            this.nUDDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nUDDistance.Location = new System.Drawing.Point(157, 32);
            this.nUDDistance.Maximum = new decimal(new int[] {
            640,
            0,
            0,
            0});
            this.nUDDistance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDDistance.Name = "nUDDistance";
            this.nUDDistance.ReadOnly = true;
            this.nUDDistance.Size = new System.Drawing.Size(121, 23);
            this.nUDDistance.TabIndex = 28;
            this.nUDDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nUDDistance.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblOptions
            // 
            this.lblOptions.BackColor = System.Drawing.Color.LightGreen;
            this.lblOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptions.Location = new System.Drawing.Point(0, 0);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(620, 20);
            this.lblOptions.TabIndex = 27;
            this.lblOptions.Text = "::Options";
            // 
            // fLPLearningSet
            // 
            this.fLPLearningSet.AutoScroll = true;
            this.fLPLearningSet.BackColor = System.Drawing.SystemColors.Control;
            this.fLPLearningSet.Dock = System.Windows.Forms.DockStyle.Top;
            this.fLPLearningSet.Location = new System.Drawing.Point(0, 20);
            this.fLPLearningSet.Name = "fLPLearningSet";
            this.fLPLearningSet.Size = new System.Drawing.Size(620, 130);
            this.fLPLearningSet.TabIndex = 6;
            // 
            // lblLearningSet
            // 
            this.lblLearningSet.BackColor = System.Drawing.Color.LightGreen;
            this.lblLearningSet.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLearningSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLearningSet.Location = new System.Drawing.Point(0, 0);
            this.lblLearningSet.Name = "lblLearningSet";
            this.lblLearningSet.Size = new System.Drawing.Size(620, 20);
            this.lblLearningSet.TabIndex = 27;
            this.lblLearningSet.Text = "::Learning Set";
            // 
            // pFundo
            // 
            this.pFundo.BackColor = System.Drawing.SystemColors.Control;
            this.pFundo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pFundo.Location = new System.Drawing.Point(0, 0);
            this.pFundo.Name = "pFundo";
            this.pFundo.Size = new System.Drawing.Size(1283, 583);
            this.pFundo.TabIndex = 3;
            // 
            // cDTrainingSet
            // 
            this.cDTrainingSet.FullOpen = true;
            this.cDTrainingSet.ShowHelp = true;
            this.cDTrainingSet.SolidColorOnly = true;
            // 
            // fTextureAnalysis1024
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1283, 701);
            this.Controls.Add(this.panelRight);
            this.Controls.Add(this.pLeft);
            this.Controls.Add(this.pFundo);
            this.Controls.Add(this.pDataBase);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "fTextureAnalysis1024";
            this.Text = "..:: GLCM Texture Analyzer 1024 ::..";
            this.Shown += new System.EventHandler(this.fTextureAnalysis1024_Shown);
            this.pDataBase.ResumeLayout(false);
            this.pDataBase.PerformLayout();
            this.pLeft.ResumeLayout(false);
            this.fLPImgToProcess.ResumeLayout(false);
            this.panelRight.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDdistanceK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDistance)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pDataBase;
        private System.Windows.Forms.Panel pLeft;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Panel pFundo;
        private System.Windows.Forms.Label lblImgDBPath;
        private System.Windows.Forms.FlowLayoutPanel fLPLearningSet;
        private System.Windows.Forms.Label lblIMGPath;
        private System.Windows.Forms.Button btnPathDB;
        private System.Windows.Forms.TextBox txtPathDB;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel fLPImageDataBasePath;
        private System.Windows.Forms.FolderBrowserDialog fBDPathDB;
        private System.Windows.Forms.ColorDialog cDTrainingSet;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblKnnDistanceK;
        private System.Windows.Forms.NumericUpDown nUDdistanceK;
        private System.Windows.Forms.Button btnCompute;
        private System.Windows.Forms.ComboBox cbKNNDistance;
        private System.Windows.Forms.Label lblKNNDistance;
        private System.Windows.Forms.ComboBox cbImgDivision;
        private System.Windows.Forms.Label lblSplitImageSize;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.NumericUpDown nUDDistance;
        private System.Windows.Forms.Label lblOptions;
        private System.Windows.Forms.Label lblLearningSet;
        private System.Windows.Forms.FlowLayoutPanel fLPImgToProcess;
        private System.Windows.Forms.Panel pImgToProcess;
        private System.Windows.Forms.Button btnClearLearningSet;
        private System.Windows.Forms.Button btnClearProcess;
        private System.Windows.Forms.ComboBox cbInternalDivision;
        private System.Windows.Forms.Label lblInternalDivision;
    }
}