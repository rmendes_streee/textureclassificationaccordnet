﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("textureClassification")]
[assembly: AssemblyDescription("This project consists in a testing set of texture classification using the Accord .Net Framework (http://accord-framework.net/) intending the possibilities that could be used to digital image processing and pattern recognition")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Services Treee - Rodrigo Mendes")]
[assembly: AssemblyProduct("Texture Classification")]
[assembly: AssemblyCopyright("Copyright © Services Treee 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("eca0abef-8d5c-4151-a4dd-be600ce37504")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
