﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing.Imaging;
using System.Drawing;
using System.Data;
using System.Numerics;
using System.Runtime.InteropServices;
using Accord.MachineLearning;
using Accord.Statistics.Filters;
using Accord.Statistics.Visualizations;
using Accord.Imaging;
using Accord.Imaging.Converters;
using Accord.Math;
using AForge.Imaging;



namespace textureClassification
{
    public enum imagePixelParts
    {
        pix640 = 1,
        pix320 = 4,
        pix160 = 16,
        pix80 = 64,
        pix40 = 256,
        pix20 = 1024,
        pix10 = 4096,
        pix5 = 16384
    }

    public enum internalParts
    {
        n1 = 1,
        n4 = 4,
        n16 = 16,
        n64 = 64,
        n256 = 256,
        n1024 = 1024,
        n4096 = 4096,
        n16384 = 16384
    }

    public enum knnDistance 
    { 
         Euclidean
        ,BrayCurtis
        ,Canberra
        ,Chessboard
        ,Correlation
        ,Cosine
        ,Manhattan
        ,SquareEuclidean
        ,Hamming
        ,Chebyshev
    }

    public enum textureTypes
    {
        ClassX,
        ClassY,
        ClassW,
        ClassZ
    };

    public class textureGLCM
    {
        #region Options
        public string dbPath { get; set; }

        public int glcmDistance { get; set; }
        public int distanceK { get; set; }

        private knnDistance knnDistanceValue { get; set; }
        public string KNNDistance { 
            get{ return knnDistanceValue.ToString(); }
            set{
                switch (value)
	            {
		            case "Euclidean":       knnDistanceValue = knnDistance.Euclidean;       break;
                    case "BrayCurtis":      knnDistanceValue = knnDistance.BrayCurtis;      break;
                    case "Canberra":        knnDistanceValue = knnDistance.Canberra;        break;
                    case "Chessboard":      knnDistanceValue = knnDistance.Chessboard;      break;
                    case "Correlation":     knnDistanceValue = knnDistance.Correlation;     break;
                    case "Cosine":          knnDistanceValue = knnDistance.Cosine;          break;
                    case "Manhattan":       knnDistanceValue = knnDistance.Manhattan;       break;
                    case "SquareEuclidean": knnDistanceValue = knnDistance.SquareEuclidean; break;
                    case "Hamming":         knnDistanceValue = knnDistance.Hamming;         break;
                    case "Chebyshev":       knnDistanceValue = knnDistance.Chebyshev;       break;
                    default:                knnDistanceValue = knnDistance.Euclidean;       break;
	            }
            } 
        }
        
        private imagePixelParts imgDivisionValue { get; set; }
        public imagePixelParts imgDivisionPixelParts { get { return imgDivisionValue; } }
        public string imgDivision { 
            get {return imgDivisionValue.ToString(); } 
            set {
                    switch (value){
		               case "pix640": imgDivisionValue = imagePixelParts.pix640; break;
                       case "pix320": imgDivisionValue = imagePixelParts.pix320; break;
                       case "pix160": imgDivisionValue = imagePixelParts.pix160; break;
                       case "pix80":  imgDivisionValue = imagePixelParts.pix80;  break;
                       case "pix40":  imgDivisionValue = imagePixelParts.pix40;  break;
                       case "pix20":  imgDivisionValue = imagePixelParts.pix20;  break;
                       case "pix10":  imgDivisionValue = imagePixelParts.pix10;  break;
                       case "pix5":   imgDivisionValue = imagePixelParts.pix5;   break;
                       default:       imgDivisionValue = imagePixelParts.pix320; break;
	                 }
            } 
        }

        private internalParts internalDivisionValue { get; set; }
        public internalParts internalDivisionParts { get { return internalDivisionValue; } }
        public string internalDivision
        {
            get { return internalDivisionValue.ToString(); }
            set
            {
                switch (value)
                {
                    case "n1": internalDivisionValue = internalParts.n1; break;
                    case "n4": internalDivisionValue = internalParts.n4; break;
                    case "n16": internalDivisionValue = internalParts.n16; break;
                    case "n64": internalDivisionValue = internalParts.n64; break;
                    case "n256": internalDivisionValue = internalParts.n256; break;
                    case "n1024": internalDivisionValue = internalParts.n1024; break;
                    case "n4096": internalDivisionValue = internalParts.n4096; break;
                    case "n16384": internalDivisionValue = internalParts.n16384; break;
                    default: internalDivisionValue = internalParts.n1; break;
                }
            }
        }
        #endregion

        public List<textureVO> learningSet { get; set; }
        public List<textureVO> toBeClassifiedSet { get; set; }

        public List<Color> colorClasses { get; set; }

        public textureGLCM()
        {
            dbPath = @"C:\brodatz\bmp";
            knnDistanceValue = knnDistance.Euclidean;
            glcmDistance = 1;
            imgDivisionValue = imagePixelParts.pix640;
            internalDivisionValue = internalParts.n1;
        }

        public void Compute()
        {
            //Prepare Learning Set
            colorClasses = new List<Color>();
            foreach (textureVO item in learningSet)
            {
                //indentify classes
                if (!colorClasses.Contains(item.color))
                    colorClasses.Add(item.color);

                item.classIndex = colorClasses.IndexOf(item.color);

                //process GLCM Descriptors
                List<byte[]> arraySplited = new List<byte[]>();
                item.imbDataSplited = splitBMP(item.imgBitmap, out arraySplited, internalDivisionValue); //REPEAT DIVISION ???
                item.arraySplited = arraySplited;
                item.glcmDescriptors = getDescriptors(item.imbDataSplited, item.arraySplited, glcmDistance);
            }

            //Prepare To Be Classified Set
            foreach (textureVO item in toBeClassifiedSet)
            {
                //process GLCM Descriptors
                List<byte[]> arraySplited = new List<byte[]>();
                item.imbDataSplited = splitBMP(item.imgBitmap, out arraySplited, internalDivisionValue); //REPEAT DIVISION ???
                item.arraySplited = arraySplited;
                item.glcmDescriptors = getDescriptors(item.imbDataSplited, item.arraySplited, glcmDistance);
            }

            //set to be compared
            List<DataTable> learningData = new List<DataTable>();
            List<int> outputs = new List<int>();
            int count = 0;
            foreach (Color item in colorClasses)
            {
                foreach (textureVO VO in learningSet)
                    if (item == VO.color)
                    {
                        outputs.Add(count);
                        learningData.Add(VO.glcmDescriptors);
                    }
                count++;
            }

            //Metrics for KNN
            Func<DataTable, DataTable, double> myDistance = delegate(DataTable p_A, DataTable p_B)
            {
                switch (knnDistanceValue)
                {
                    case knnDistance.Euclidean:
                        return Distance.Euclidean(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.BrayCurtis:
                        return Distance.BrayCurtis(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Canberra:
                        return Distance.Canberra(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Chessboard:
                        return Distance.Chessboard(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Correlation:
                        return Distance.Correlation(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Cosine:
                        return Distance.Cosine(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Manhattan:
                        return Distance.Manhattan(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.SquareEuclidean:
                        return Distance.SquareEuclidean(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Hamming:
                        return Distance.Hamming(normalizeDt(p_A), normalizeDt(p_B));
                    case knnDistance.Chebyshev:
                        return Distance.Chebyshev(normalizeDt(p_A), normalizeDt(p_B));
                    default:
                        return Distance.Euclidean(normalizeDt(p_A), normalizeDt(p_B));
                }
            };


            KNearestNeighbors<DataTable> knn = new KNearestNeighbors<DataTable>(distanceK, learningData.ToArray(), outputs.ToArray(), myDistance);
            foreach (textureVO item in toBeClassifiedSet)
            {
                item.classIndex = knn.Compute(item.glcmDescriptors);
            }

        }

        private static void LockUnlockBitsExample()
        {

            // Create a new bitmap.
            Bitmap bmp = new Bitmap(@"C:\brodatz\bmp\a\A-1 (" + 1 + ").bmp");

            // Lock the bitmap's bits.  
            Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Imaging.BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes  = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            // Set every third value to 255. A 24bpp bitmap will look red.  
            for (int counter = 2; counter < rgbValues.Length; counter += 3)
                rgbValues[counter] = 255;

            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);

            // Draw the modified image.
            //e.Graphics.DrawImage(bmp, 0, 150);

        }

        //public static void ComputeTESTE()
        //{
        //    LockUnlockBitsExample();

        //    UnmanagedImage[] result5 = new UnmanagedImage[16];
        //    BitmapData[] resultBMP = new BitmapData[16];
        //    List<byte[]> arraysBitmaData = new List<byte[]>();
        //    for (int i = 0; i < 16; i++)
        //    {
        //        Bitmap p_img = new Bitmap(@"C:\brodatz\jpg\A-1 (" + (i + 1) + ").jpg");
        //        Rectangle rect = new Rectangle(0, 0, 640, 640);
        //        Bitmap clone = p_img.Clone(rect, p_img.PixelFormat);
        //        BitmapData bmpData = clone.LockBits(rect, ImageLockMode.ReadWrite, clone.PixelFormat);

        //        result5[i] = new UnmanagedImage(bmpData);
        //        resultBMP[i] = bmpData;
        //        arraysBitmaData.Add(getArrayImageData(result5[i]));
        //    }
        //    DataTable inCompareSplited2 = new DataTable();
        //    inCompareSplited2 = getDescriptors(result5, arraysBitmaData);



        //    Bitmap bmpSample = new Bitmap(@"C:\brodatz\bmp\D28.bmp");
        //    List<byte[]> arraysBmpSplited = new List<byte[]>();
        //    UnmanagedImage[] bmpSplited = splitBMP(bmpSample, out arraysBmpSplited, imagePixelParts.pix160);

        //    DataTable inCompareSplited = new DataTable();
        //    inCompareSplited = getDescriptors(bmpSplited, arraysBmpSplited);


        //    Training set
        //    DataTable inCirc = new DataTable();
        //    DataTable inLine = new DataTable();
        //    DataTable inMesh = new DataTable();
        //    DataTable inOthe = new DataTable();
            
        //    bmpSample = new Bitmap(@"C:\brodatz\bmp\D28.bmp");
        //    arraysBmpSplited = new List<byte[]>();
        //    bmpSplited = splitBMP(bmpSample, out arraysBmpSplited, imagePixelParts.pix160);
        //    inCirc = getDescriptors(bmpSplited, arraysBmpSplited, 5);

        //    bmpSample = new Bitmap(@"C:\brodatz\bmp\D15.bmp");
        //    arraysBmpSplited = new List<byte[]>();
        //    bmpSplited = splitBMP(bmpSample, out arraysBmpSplited, imagePixelParts.pix160);
        //    inLine = getDescriptors(bmpSplited, arraysBmpSplited, 5);

        //    bmpSample = new Bitmap(@"C:\brodatz\bmp\D22.bmp");
        //    arraysBmpSplited = new List<byte[]>();
        //    bmpSplited = splitBMP(bmpSample, out arraysBmpSplited, imagePixelParts.pix160);
        //    inMesh = getDescriptors(bmpSplited, arraysBmpSplited, 5);

        //    bmpSample = new Bitmap(@"C:\brodatz\bmp\D24.bmp");
        //    arraysBmpSplited = new List<byte[]>();
        //    bmpSplited = splitBMP(bmpSample, out arraysBmpSplited, imagePixelParts.pix160);
        //    inOthe = getDescriptors(bmpSplited, arraysBmpSplited, 5);

        //    inCirc = getDescriptors(@"C:\brodatz\bmp\D28.bmp");
        //    inLine = getDescriptors(@"C:\brodatz\bmp\D15.bmp");
        //    inMesh = getDescriptors(@"C:\brodatz\bmp\D22.bmp");
        //    inOthe = getDescriptors(@"C:\brodatz\bmp\D24.bmp");


        //    set to be compared
        //    bmpSample = new Bitmap(@"C:\brodatz\bmp\D24.bmp");
        //    arraysBmpSplited = new List<byte[]>();
        //    bmpSplited = splitBMP(bmpSample, out arraysBmpSplited, imagePixelParts.pix160);
        //    DataTable inCompare = new DataTable();
        //    inCompare = getDescriptors(bmpSplited, arraysBmpSplited, 5);
            
        //    inCompare = getDescriptors(@"C:\brodatz\bmp\D57.bmp");

        //    learning data
        //    DataTable[] learningData = new DataTable[4];
        //    learningData[0] = inCirc;
        //    learningData[1] = inLine;
        //    learningData[2] = inMesh;
        //    learningData[3] = inOthe;

        //    int[] outputs =
        //    {
        //        (int)textureTypes.ClassX, //0, 
        //        (int)textureTypes.ClassY, //1, 
        //        (int)textureTypes.ClassW, //2
        //        (int)textureTypes.ClassZ  //3
        //    };

        //    Metrics for KNN
        //    Func<DataTable, DataTable, double> myDistance = delegate(DataTable p_A, DataTable p_B)
        //    {
        //        return Distance.Euclidean(normalizeDt(p_A), normalizeDt(p_B));
        //    };

        //    KNearestNeighbors<DataTable> knn = new KNearestNeighbors<DataTable>(4, learningData, outputs, myDistance);
        //    textureTypes resType = (textureTypes)knn.Compute(inCompare);

        //}

        public static UnmanagedImage[] splitBMP(Bitmap p_img, out List<byte[]> arraysBitmaData, internalParts numParts = internalParts.n1)
        {
            arraysBitmaData = new List<byte[]>();
            UnmanagedImage[] result = new UnmanagedImage[(int)numParts];
            int x = 0;
            int y = 0;
            int width = p_img.Width / Convert.ToInt32(Math.Sqrt((int)numParts));
            int height = p_img.Height / Convert.ToInt32(Math.Sqrt((int)numParts));

            for (int i = 0; i < (int)numParts; i++)
            {
                //verifica se é última parte se for diminui 1 tamanho
                //Rectangle rect = new Rectangle(x, y, ((width+x)>p_img.Width) ? width-1 : width, ((height+y)>p_img.Height) ? height-1 : height);
                Rectangle rect = new Rectangle(x, y, width, height);
                Bitmap clone = p_img.Clone(rect, p_img.PixelFormat);
                

                //reseta posições para transformar o clone da parte da imagem em BitmapData
                rect.X = 0;
                rect.Y = 0;
                BitmapData bmpData = clone.LockBits(rect, ImageLockMode.ReadWrite, clone.PixelFormat);
                result[i] = new UnmanagedImage(bmpData);
                arraysBitmaData.Add(getArrayImageData(result[i]));

                x = ((i > 0) && ((y + width) == p_img.Width)) ? x + width : x;
                y = ((i > 0) && ((y + width) == p_img.Width)) ? 0 : y + width;
            }
            return result;
        }

        public static double[] normalizeDt(DataTable p_dt)
        {
            if (p_dt != null)
            {
                double[] normalized, temp;
                normalized = new double[0];
                double max = 0;
                for (int i = 0; i < p_dt.Columns.Count; i++)
                {
                    temp = p_dt.AsEnumerable()
                          .Select(row => Convert.ToDouble(row.Field<double>(p_dt.Columns[i].ColumnName), System.Globalization.CultureInfo.InvariantCulture)).ToArray();

                    //Normalize Column values -- y = x - min / max - min <==> y = x / max
                    max = temp.Max();
                    temp = p_dt.AsEnumerable()
                          .Select(row => Convert.ToDouble(row.Field<double>(p_dt.Columns[i].ColumnName) / max, System.Globalization.CultureInfo.InvariantCulture)).ToArray();
                    normalized = normalized.Concat(temp).ToArray();
                }
                return normalized;
            }
            else
                return null;
        }

        //public static DataTable getDescriptors(UnmanagedImage[] img, List<byte[]> vectorBitmapData, int p_distance = 1)
        //{
        //    DataTable dt = new DataTable("dataGLCM");
        //    dt.Columns.Add("Entropy", typeof(double));
        //    dt.Columns.Add("TextureHomogeneity", typeof(double));
        //    dt.Columns.Add("Contrast", typeof(double));
        //    dt.Columns.Add("Correlation", typeof(double));

        //    myGrayLevelCooccurrenceMatrix glcm;
        //    double[,] glcmMatrix;
        //    HaralickDescriptor desc;
        //    //HaralickDescriptorDictionary dd = new HaralickDescriptorDictionary();

        //    for (int i = 0; i < img.Count(); i++)
        //    {
        //        glcm = new myGrayLevelCooccurrenceMatrix();
        //        glcm.Degree = myCooccurrenceDegree.Degree0; //--> 0º
        //        glcm.Distance = p_distance;
        //        glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
        //        desc = new HaralickDescriptor(glcmMatrix);
        //        dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);


        //        glcm = new myGrayLevelCooccurrenceMatrix();
        //        glcm.Degree = myCooccurrenceDegree.Degree45; //--> 45º
        //        glcm.Distance = p_distance;
        //        glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
        //        desc = new HaralickDescriptor(glcmMatrix);
        //        dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);


        //        glcm = new myGrayLevelCooccurrenceMatrix();
        //        glcm.Degree = myCooccurrenceDegree.Degree90; //--> 90º
        //        glcm.Distance = p_distance;
        //        glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
        //        desc = new HaralickDescriptor(glcmMatrix);
        //        dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);


        //        glcm = new myGrayLevelCooccurrenceMatrix();
        //        glcm.Degree = myCooccurrenceDegree.Degree135; //--> 135º
        //        glcm.Distance = p_distance;
        //        glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
        //        desc = new HaralickDescriptor(glcmMatrix);
        //        dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);
        //    }
        //    return dt;
        //}

        //private static byte[] LockUnlockBitsExample(BitmapData bmpData)
        //{
        //    // Get the address of the first line.
        //    IntPtr ptr = bmpData.Scan0;

        //    // Declare an array to hold the bytes of the bitmap.
        //    int bytes = Math.Abs(bmpData.Stride) * bmpData.Height;
        //    byte[] rgbValues = new byte[bytes];

        //    // Copy the RGB values into the array.
        //    System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

        //    return rgbValues;
        //}

        //private static byte[] getArrayImageData(UnmanagedImage bmpData)
        //{
        //    // Get the address of the first line.
        //    IntPtr ptr = bmpData.ImageData;

        //    // Declare an array to hold the bytes of the bitmap.
        //    int bytes = Math.Abs(bmpData.Stride) * bmpData.Height;
        //    byte[] rgbValues = new byte[bytes];

        //    // Copy the RGB values into the array.
        //    System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

        //    return rgbValues;
        //}

        public static DataTable getDescriptors(UnmanagedImage[] img, List<byte[]> vectorBitmapData, int p_distance = 1)
        {
            DataTable dt = new DataTable("dataGLCM");
            dt.Columns.Add("Entropy", typeof(double));
            dt.Columns.Add("TextureHomogeneity", typeof(double));
            dt.Columns.Add("Contrast", typeof(double));
            dt.Columns.Add("Correlation", typeof(double));

            myGrayLevelCooccurrenceMatrix glcm;
            double[,] glcmMatrix;
            myHaralickDescriptor desc;
            //HaralickDescriptorDictionary dd = new HaralickDescriptorDictionary();

            for (int i = 0; i < img.Count(); i++)
            {
                glcm = new myGrayLevelCooccurrenceMatrix();
                glcm.Degree = myCooccurrenceDegree.Degree0; //--> 0º
                glcm.Distance = p_distance;
                glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
                desc = new myHaralickDescriptor(glcmMatrix);
                dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);


                glcm = new myGrayLevelCooccurrenceMatrix();
                glcm.Degree = myCooccurrenceDegree.Degree45; //--> 45º
                glcm.Distance = p_distance;
                glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
                desc = new myHaralickDescriptor(glcmMatrix);
                dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);


                glcm = new myGrayLevelCooccurrenceMatrix();
                glcm.Degree = myCooccurrenceDegree.Degree90; //--> 90º
                glcm.Distance = p_distance;
                glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
                desc = new myHaralickDescriptor(glcmMatrix);
                dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);


                glcm = new myGrayLevelCooccurrenceMatrix();
                glcm.Degree = myCooccurrenceDegree.Degree135; //--> 135º
                glcm.Distance = p_distance;
                glcmMatrix = glcm.Compute(img[i], vectorBitmapData[i]);
                desc = new myHaralickDescriptor(glcmMatrix);
                dt.Rows.Add(desc.Entropy, desc.TextureHomogeneity, desc.Contrast, desc.Correlation);
            }
            return dt;
        }

        private static byte[] getArrayImageData(UnmanagedImage bmpData)
        {
            // Get the address of the first line.
            IntPtr ptr = bmpData.ImageData;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmpData.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            return rgbValues;
        }
        
    }
}