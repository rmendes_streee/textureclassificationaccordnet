﻿namespace textureClassification
{
    partial class fImgToProcess
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fBDPathDB = new System.Windows.Forms.FolderBrowserDialog();
            this.lblImgDBPath = new System.Windows.Forms.Label();
            this.btnCompute = new System.Windows.Forms.Button();
            this.cbKNNDistance = new System.Windows.Forms.ComboBox();
            this.lblKNNDistance = new System.Windows.Forms.Label();
            this.pImgToProcess = new System.Windows.Forms.Panel();
            this.cbImgDivision = new System.Windows.Forms.ComboBox();
            this.lblDistance = new System.Windows.Forms.Label();
            this.nUDDistance = new System.Windows.Forms.NumericUpDown();
            this.lblOptions = new System.Windows.Forms.Label();
            this.lblLearningSet = new System.Windows.Forms.Label();
            this.cDTrainingSet = new System.Windows.Forms.ColorDialog();
            this.panelRight = new System.Windows.Forms.Panel();
            this.lblKnnDistanceK = new System.Windows.Forms.Label();
            this.nUDdistanceK = new System.Windows.Forms.NumericUpDown();
            this.lblSplitImageSize = new System.Windows.Forms.Label();
            this.fLPLearningSet = new System.Windows.Forms.FlowLayoutPanel();
            this.pCentral = new System.Windows.Forms.Panel();
            this.pImgToProcess.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDistance)).BeginInit();
            this.panelRight.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDdistanceK)).BeginInit();
            this.pCentral.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblImgDBPath
            // 
            this.lblImgDBPath.BackColor = System.Drawing.Color.LightGreen;
            this.lblImgDBPath.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblImgDBPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImgDBPath.Location = new System.Drawing.Point(0, 0);
            this.lblImgDBPath.Name = "lblImgDBPath";
            this.lblImgDBPath.Size = new System.Drawing.Size(761, 26);
            this.lblImgDBPath.TabIndex = 1;
            this.lblImgDBPath.Text = "::Image To Process";
            // 
            // btnCompute
            // 
            this.btnCompute.AccessibleDescription = "Compute classification description";
            this.btnCompute.BackColor = System.Drawing.SystemColors.Control;
            this.btnCompute.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnCompute.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnCompute.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompute.Location = new System.Drawing.Point(36, 445);
            this.btnCompute.Name = "btnCompute";
            this.btnCompute.Size = new System.Drawing.Size(245, 52);
            this.btnCompute.TabIndex = 14;
            this.btnCompute.Tag = "Compute classification Tag";
            this.btnCompute.Text = "PROCESS";
            this.btnCompute.UseVisualStyleBackColor = false;
            this.btnCompute.Click += new System.EventHandler(this.btnCompute_Click);
            // 
            // cbKNNDistance
            // 
            this.cbKNNDistance.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbKNNDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbKNNDistance.FormattingEnabled = true;
            this.cbKNNDistance.Items.AddRange(new object[] {
            "Euclidean",
            "BrayCurtis",
            "Canberra",
            "Chessboard",
            "Correlation",
            "Cosine",
            "Manhattan",
            "SquareEuclidean",
            "Hamming",
            "Chebyshev"});
            this.cbKNNDistance.Location = new System.Drawing.Point(173, 340);
            this.cbKNNDistance.Name = "cbKNNDistance";
            this.cbKNNDistance.Size = new System.Drawing.Size(121, 28);
            this.cbKNNDistance.TabIndex = 13;
            // 
            // lblKNNDistance
            // 
            this.lblKNNDistance.AutoSize = true;
            this.lblKNNDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKNNDistance.Location = new System.Drawing.Point(3, 345);
            this.lblKNNDistance.Name = "lblKNNDistance";
            this.lblKNNDistance.Size = new System.Drawing.Size(163, 20);
            this.lblKNNDistance.TabIndex = 12;
            this.lblKNNDistance.Text = "KNN Distance Type";
            // 
            // pImgToProcess
            // 
            this.pImgToProcess.Controls.Add(this.lblImgDBPath);
            this.pImgToProcess.Dock = System.Windows.Forms.DockStyle.Left;
            this.pImgToProcess.Location = new System.Drawing.Point(0, 0);
            this.pImgToProcess.Name = "pImgToProcess";
            this.pImgToProcess.Size = new System.Drawing.Size(761, 762);
            this.pImgToProcess.TabIndex = 5;
            // 
            // cbImgDivision
            // 
            this.cbImgDivision.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbImgDivision.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbImgDivision.FormattingEnabled = true;
            this.cbImgDivision.Items.AddRange(new object[] {
            "pix640",
            "pix320",
            "pix160",
            "pix80",
            "pix40",
            "pix20",
            "pix10",
            "pix5"});
            this.cbImgDivision.Location = new System.Drawing.Point(174, 299);
            this.cbImgDivision.Name = "cbImgDivision";
            this.cbImgDivision.Size = new System.Drawing.Size(121, 28);
            this.cbImgDivision.TabIndex = 11;
            this.cbImgDivision.SelectedIndexChanged += new System.EventHandler(this.cbImgDivision_SelectedIndexChanged);
            // 
            // lblDistance
            // 
            this.lblDistance.AutoSize = true;
            this.lblDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDistance.Location = new System.Drawing.Point(32, 262);
            this.lblDistance.Name = "lblDistance";
            this.lblDistance.Size = new System.Drawing.Size(135, 20);
            this.lblDistance.TabIndex = 9;
            this.lblDistance.Text = "GLCM Distance";
            // 
            // nUDDistance
            // 
            this.nUDDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nUDDistance.Location = new System.Drawing.Point(173, 260);
            this.nUDDistance.Maximum = new decimal(new int[] {
            640,
            0,
            0,
            0});
            this.nUDDistance.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDDistance.Name = "nUDDistance";
            this.nUDDistance.ReadOnly = true;
            this.nUDDistance.Size = new System.Drawing.Size(121, 26);
            this.nUDDistance.TabIndex = 8;
            this.nUDDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nUDDistance.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblOptions
            // 
            this.lblOptions.BackColor = System.Drawing.Color.LightGreen;
            this.lblOptions.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptions.Location = new System.Drawing.Point(0, 223);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(637, 26);
            this.lblOptions.TabIndex = 6;
            this.lblOptions.Text = "::Options";
            // 
            // lblLearningSet
            // 
            this.lblLearningSet.BackColor = System.Drawing.Color.LightGreen;
            this.lblLearningSet.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblLearningSet.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLearningSet.Location = new System.Drawing.Point(0, 0);
            this.lblLearningSet.Name = "lblLearningSet";
            this.lblLearningSet.Size = new System.Drawing.Size(637, 26);
            this.lblLearningSet.TabIndex = 4;
            this.lblLearningSet.Text = "::Learning Set";
            // 
            // cDTrainingSet
            // 
            this.cDTrainingSet.FullOpen = true;
            this.cDTrainingSet.ShowHelp = true;
            this.cDTrainingSet.SolidColorOnly = true;
            // 
            // panelRight
            // 
            this.panelRight.Controls.Add(this.lblKnnDistanceK);
            this.panelRight.Controls.Add(this.nUDdistanceK);
            this.panelRight.Controls.Add(this.btnCompute);
            this.panelRight.Controls.Add(this.cbKNNDistance);
            this.panelRight.Controls.Add(this.lblKNNDistance);
            this.panelRight.Controls.Add(this.cbImgDivision);
            this.panelRight.Controls.Add(this.lblSplitImageSize);
            this.panelRight.Controls.Add(this.lblDistance);
            this.panelRight.Controls.Add(this.nUDDistance);
            this.panelRight.Controls.Add(this.lblOptions);
            this.panelRight.Controls.Add(this.fLPLearningSet);
            this.panelRight.Controls.Add(this.lblLearningSet);
            this.panelRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelRight.Location = new System.Drawing.Point(761, 0);
            this.panelRight.Name = "panelRight";
            this.panelRight.Size = new System.Drawing.Size(637, 762);
            this.panelRight.TabIndex = 6;
            // 
            // lblKnnDistanceK
            // 
            this.lblKnnDistanceK.AutoSize = true;
            this.lblKnnDistanceK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKnnDistanceK.Location = new System.Drawing.Point(70, 387);
            this.lblKnnDistanceK.Name = "lblKnnDistanceK";
            this.lblKnnDistanceK.Size = new System.Drawing.Size(96, 20);
            this.lblKnnDistanceK.TabIndex = 16;
            this.lblKnnDistanceK.Text = "Distance K";
            // 
            // nUDdistanceK
            // 
            this.nUDdistanceK.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nUDdistanceK.Location = new System.Drawing.Point(173, 385);
            this.nUDdistanceK.Maximum = new decimal(new int[] {
            640,
            0,
            0,
            0});
            this.nUDdistanceK.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDdistanceK.Name = "nUDdistanceK";
            this.nUDdistanceK.ReadOnly = true;
            this.nUDdistanceK.Size = new System.Drawing.Size(121, 26);
            this.nUDdistanceK.TabIndex = 15;
            this.nUDdistanceK.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.nUDdistanceK.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lblSplitImageSize
            // 
            this.lblSplitImageSize.AutoSize = true;
            this.lblSplitImageSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSplitImageSize.Location = new System.Drawing.Point(5, 302);
            this.lblSplitImageSize.Name = "lblSplitImageSize";
            this.lblSplitImageSize.Size = new System.Drawing.Size(166, 20);
            this.lblSplitImageSize.TabIndex = 10;
            this.lblSplitImageSize.Text = "Image Division Size";
            // 
            // fLPLearningSet
            // 
            this.fLPLearningSet.AutoScroll = true;
            this.fLPLearningSet.Dock = System.Windows.Forms.DockStyle.Top;
            this.fLPLearningSet.Location = new System.Drawing.Point(0, 26);
            this.fLPLearningSet.Name = "fLPLearningSet";
            this.fLPLearningSet.Size = new System.Drawing.Size(637, 197);
            this.fLPLearningSet.TabIndex = 5;
            // 
            // pCentral
            // 
            this.pCentral.Controls.Add(this.panelRight);
            this.pCentral.Controls.Add(this.pImgToProcess);
            this.pCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pCentral.Location = new System.Drawing.Point(0, 0);
            this.pCentral.Name = "pCentral";
            this.pCentral.Size = new System.Drawing.Size(1398, 762);
            this.pCentral.TabIndex = 6;
            // 
            // fImgToProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1398, 762);
            this.Controls.Add(this.pCentral);
            this.Name = "fImgToProcess";
            this.Text = "..:: Image to Process ::..";
            this.Shown += new System.EventHandler(this.fImgToProcess_Shown);
            this.pImgToProcess.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nUDDistance)).EndInit();
            this.panelRight.ResumeLayout(false);
            this.panelRight.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDdistanceK)).EndInit();
            this.pCentral.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog fBDPathDB;
        private System.Windows.Forms.Label lblImgDBPath;
        private System.Windows.Forms.Button btnCompute;
        private System.Windows.Forms.ComboBox cbKNNDistance;
        private System.Windows.Forms.Label lblKNNDistance;
        private System.Windows.Forms.Panel pImgToProcess;
        private System.Windows.Forms.ComboBox cbImgDivision;
        private System.Windows.Forms.Label lblDistance;
        private System.Windows.Forms.NumericUpDown nUDDistance;
        private System.Windows.Forms.Label lblOptions;
        private System.Windows.Forms.Label lblLearningSet;
        private System.Windows.Forms.ColorDialog cDTrainingSet;
        private System.Windows.Forms.Panel panelRight;
        private System.Windows.Forms.Label lblSplitImageSize;
        private System.Windows.Forms.FlowLayoutPanel fLPLearningSet;
        private System.Windows.Forms.Panel pCentral;
        private System.Windows.Forms.Label lblKnnDistanceK;
        private System.Windows.Forms.NumericUpDown nUDdistanceK;


    }
}